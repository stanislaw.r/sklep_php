<?php
session_start();
require_once "systemClass.php";
require_once "layoutClass.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    SystemClass::return_head("Sklep", "static/main.css");
    ?>
</head>

<body>
    <?php
    LayoutClass::return_header();
    ?>

    <div class="shop_main">
        <div class="aside">
            <div class=aside__container>
                <?php
                LayoutClass::print_menu_left();
                ?>
            </div>
        </div>

        <div class="section">
            <div class="section__container">
            <?php
            LayoutClass::getProducts();
            ?>
            </div>
        </div>
    </div>

    <?php
    LayoutClass::return_footer();
    ?>
</body>

</html>