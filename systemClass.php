<?php
class SystemClass
{

    public static function return_head($title, $stylesheet)
    {
        echo "
        <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <title>$title</title>
        <link rel='stylesheet' href=$stylesheet>
        ";
    }

    public static function db_connect(){
        $host = "localhost";
        $db_user = "root";
        $db_password = "";
        $db_name = "sklep_php";
        return new mysqli($host, $db_user, $db_password, $db_name);
    }

    public static function blockEntranceWhenNotSignedIn($location){
        if (!isset($_SESSION["signed_in"])){
            header("location: $location");
        }
    }

    public static function blockEntranceWhenSignedIn($location){
        if (isset($_SESSION["signed_in"])){
            header("location: $location");
        }
    }

    public static function blockEntranceWhenNotNewlyCreated($location){
        if (!isset($_SESSION["new_user"])){
            header("location: $location");
        }
    }
}
?>