<?php
session_start();
require_once "systemClass.php";

$connection = SystemClass::db_connect();

if ($connection -> connect_errno === 0){
    $userEmail = htmlentities($_POST["email"], ENT_QUOTES, "UTF-8");
    $userPassword = htmlentities($_POST["password"], ENT_QUOTES, "UTF-8");

    $sql = sprintf(
        "SELECT * FROM users WHERE userEmail='%s' AND userPassword='%s'",
        mysqli_real_escape_string($connection, $userEmail),
        mysqli_real_escape_string($connection, $userPassword)
    );

    if ($result = $connection->query($sql)){
        if ($result->num_rows > 0){
            $data = $result->fetch_assoc();
            $id = $data['userId'];
            $username = $data['userName'];
            $email = $data['userEmail'];
            $_SESSION['signed_in'] = TRUE;
            unset($_SESSION['signinError']);
            header("Location: index.php"); 
        } else {
            $_SESSION['signinError'] = TRUE;
            header('Location: signin_page.php');
        }
        $result->close();
    }

    $connection->close();
}
?>