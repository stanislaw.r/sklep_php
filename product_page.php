<?php
session_start();
require_once "systemClass.php";
require_once "layoutClass.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    SystemClass::return_head("Sklep", "static/main.css");
    ?>
</head>

<body>
    <?php
    LayoutClass::return_header();
    ?>

    <div class="product">
        <?php
        LayoutClass::showProduct();
        ?>
    </div>

    <?php
    LayoutClass::return_footer();
    ?>
</body>

</html>