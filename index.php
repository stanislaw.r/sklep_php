<?php
session_start();
require_once "systemClass.php";
require_once "layoutClass.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    SystemClass::return_head("Sklep", "static/main.css");
    ?>
</head>

<body>
    <?php
    LayoutClass::return_header();
    ?>

    <div class="hero">
        <div class="hero__container">
            <h1>Sklep Internetowy</h1>
            <p><a href="shop_page.php">Produkty</a></p>
        </div>
    </div>

    <div class="cards">
        <div class="cards__container">
            <h1>c1</h1>
        </div>
        <div class="cards__container">
            <h1>c1</h1>
        </div>
        <div class="cards__container">
            <h1>c1</h1>
        </div>
    </div>

    <?php
    LayoutClass::return_footer();
    ?>
</body>

</html>