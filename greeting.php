<?php
session_start();
require_once "systemClass.php";
require_once "layoutClass.php";

SystemClass::blockEntranceWhenNotNewlyCreated("index.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    SystemClass::return_head("Welcome!", "static/main.css");
    ?>
</head>

<body>
    <?php
    LayoutClass::return_header();
    ?>

    <div class="hero">
        <div class="hero__container">
            <h1>Welcome!</h1>
        </div>
    </div>

    <?php
    LayoutClass::return_footer();
    ?>
</body>

</html>