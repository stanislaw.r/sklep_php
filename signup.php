<?php
session_start();
require_once "systemClass.php";

$connection = SystemClass::db_connect();

if ($connection->connect_errno == 0) {
    $userName = htmlentities($_POST['name'], ENT_QUOTES, "UTF-8");
    $userEmail = htmlentities($_POST['email'], ENT_QUOTES, "UTF-8");
    $userPassword = htmlentities($_POST['password'], ENT_QUOTES, "UTF-8");
    $userPassword_Repeated = htmlentities($_POST['password_rep'], ENT_QUOTES, "UTF-8");
    $agreed = htmlentities($_POST['terms'], ENT_QUOTES, "UTF-8");
    $validation_failed = FALSE;
    if (strlen($userPassword) < 8) {
        session_unset();
        $validation_failed = TRUE;
        $_SESSION['signupError'] = 'Hasło musi mieć przynajmniej 8 znaków!';
    }
    if ($userPassword != $userPassword_Repeated) {
        session_unset();
        $validation_failed = TRUE;
        $_SESSION['signupError'] = 'Hasła muszą być identyczne!';
    }
    if (!$agreed) {
        session_unset();
        $validation_failed = TRUE;
        $_SESSION['signupError'] = 'Wymagana zgoda na warunki!';
    }

    $sql = sprintf(
        "INSERT into users (userName, userEmail, userPassword) values ('%s', '%s', '%s')",
        mysqli_real_escape_string($connection, $userName),
        mysqli_real_escape_string($connection, $userEmail),
        mysqli_real_escape_string($connection, $userPassword)
    );
    if ($validation_failed === FALSE) {
        if ($connection->query($sql) === TRUE) {
            session_unset();
            $_SESSION['user'] = $user;
            $_SESSION['signed_in'] = true;
            $_SESSION['new_user'] = true;
            header('Location: greeting.php');
        } else {
            session_unset();
            $_SESSION['signin_warning'] = 'Wystąpił błąd przy wysyłaniu danych!';
            header('Location: signup_page.php');
        }
    } else {
        header('Location: signup_page.php');
    }


    $connection->close();
}
