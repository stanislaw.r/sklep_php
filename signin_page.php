<?php
session_start();
require_once "systemClass.php";
require_once "layoutClass.php";
SystemClass::blockEntranceWhenSignedIn("index.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    SystemClass::return_head("Logowanie", "static/signup.css");
    ?>
</head>

<body>
<?php
    LayoutClass::return_header();
    ?>
    <div class="register">
    <form action="signin.php" method="post" class = "register__container">
    <h1>Sign in</h1>
        <ul>
            <li><label>Email</label>
            <input class="form_black_text" type="email" name="email" id=""/></li>
            <li><label>Password</label>
            <input class="form_black_text" type="password" name="password" id=""/></li>
            <li>
                <p><?php
                        if (isset($_SESSION['signinError'])){
                            echo $_SESSION['signinError'];
                        }
                        ?></p>
            </li>
            <li><input class="form_black_text" type="submit" value="Sign in"/></li>
        </ul>
    </form>
    </div>
    <?php
    LayoutClass::return_footer();
    ?>
    
</body>
</html>