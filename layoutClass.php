<?php
require_once "systemClass.php";

class LayoutClass
{
    public static function return_header()
    {
        $condition_render = "";
        if (isset($_SESSION["signed_in"]) && $_SESSION["signed_in"] === true) {
            $condition_render = "
            <li><a href='logout.php'>Wyloguj</a></li>
            ";
        } else {
            $condition_render = "
            <li><a href='signin_page.php'>Logowanie</a></li>
            <li><a href='signup_page.php'>Rejestracja</a></li>
            ";
        }
        echo "
        <header>
        <div class='header__container'>
            <h1>Logo</h1>
            <nav>
                <ul>
                    <li><a href='index.php'>Home</a></li>
                    <li><a>About</a></li>
                    <li><a>Contact</a></li>
                    <li><a href='shop_page.php'>Shop</a></li>
                    $condition_render
                </ul>
            </nav>
        </div>
    </header>
        ";
    }

    public static function return_footer()
    {
        echo "<footer>
        <div class='footer__container'>
        <h1>s</h1>
            <nav>
                <ul>
                    <li><a href=''>test1</a></li>
                    <li><a href=''>test2</a></li>
                    <li><a href=''>test3</a></li>
                    <li><a href=''>test4</a></li> 
                </ul>
            </nav>
        </div>
        </footer>";
    }

    public static function printTile($row){
        $name = $row['name'];
        $desc = $row['desc'];
        $price = $row['price'];
        $img = $row['img'];
        $id = $row['id'];

        echo "
        <div class='tile'>
        <a href='product_page.php?product_id=$id'>
            <img src='' alt='img'>
        </a>
        <ul>
            <li>$name</li>
            <li>$desc</li>
            <li>$price</li>
        </ul>
        </div>
        ";
    }

    public static function getProducts()
    {
        $connection = SystemClass::db_connect();

        $sql = "SELECT * FROM product";

        $result = mysqli_query($connection, $sql);
        while ($row = mysqli_fetch_assoc($result)){
            LayoutClass::printTile($row);
        }
    }

    public static function showProduct()
    {
        $connection = SystemClass::db_connect();

        $product_id = $_REQUEST['product_id'];

        $sql = "SELECT * FROM product WHERE id=$product_id";

        $result = mysqli_query($connection, $sql);

        $row = mysqli_fetch_assoc($result);

        $name = $row['name'];
        $desc = $row['desc'];
        $price = $row['price'];
        $img = $row['img'];

        echo "
        <div class='product__container'>
            <img src='' alt='img'>
        </div>
        <div class='product__container'>
            <h1>Product information</h1>
            <ul>
                <li>$name</li>
                <li>$price</li>
            </ul>

            <input class='' type='submit' value='Add to cart'></input>
        </div>
        ";
    }

    public static function print_menu_left()
    {
        $connection = SystemClass::db_connect();
        $sql = "SELECT 
            c1.id as one_id, c1.name as one_name, 
            c2.id as two_id, c2.name as two_name, 
            c3.id as three_id, c3.name as three_name 
            FROM category_one c1 
            left join category_two c2 on c2.id_category_one = c1.id 
            left join category_three c3 on c3.id_category_two = c2.id"; 

        $res = mysqli_query($connection, $sql);
        $menu = array();

        while ($row = mysqli_fetch_assoc($res)){
            if(!isset($menu[$row["one_id"]])){

                $menu[$row["one_id"]] = array(
                    "id" => $row["one_id"],
                    "name" => $row["one_name"],
                    "category" => array()
                );
            }

            if(!isset($menu[$row["one_id"]]["category"][$row["two_id"]])){
                $menu[$row["one_id"]]["category"][$row["two_id"]] = array(
                    "id" => $row["two_id"],
                    "name" => $row["two_name"],
                    "subcategory" => array()
                );
            }

            if(!isset($menu[$row["one_id"]]["category"][$row["two_id"]]["subcategory"][$row["three_id"]])){
                $menu[$row["one_id"]]["category"][$row["two_id"]]["subcategory"][$row["three_id"]] = array(
                    "id" => $row["three_id"],
                    "name" => $row["three_name"],
                );
            }
        }

        echo "<aside>";

        foreach($menu as $id1=>$level1){
            ?><h3>
                <a href="shop_page.php?level1=<?php echo $id1;?>&page=1">
                    <?php echo $level1['name']; ?>
                </a>
            </h3><?php

            foreach($level1["category"] as $id2=>$level2){
                ?><h4>
                    <a href="shop_page.php?level1=<?php echo $id1?>&level2=<?php echo $id2?>&page=1">
                        <?php echo $level2['name']; ?>
                    </a>
            </h4><?php

                foreach($level2["subcategory"] as $id3=>$level3){
                    ?><p>
                        <a href="shop_page.php?level1=<?php echo $id1?>&level2=<?php echo $id2?>&level3=<?php echo $id3?>&page=1">
                            <?php echo $level3['name']; ?>
                        </a>
                </p><?php
                }
            }
        }

        echo "</aside>";
        
        
    }
}
?>