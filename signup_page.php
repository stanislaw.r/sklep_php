<?php
session_start();
require_once "systemClass.php";
require_once "layoutClass.php";
SystemClass::blockEntranceWhenSignedIn("index.php");
?>

<!DOCTYPE html> 
<html lang="en">
<head>
    <?php
    SystemClass::return_head("Rejestracja", "static/signup.css");
    ?>
</head>

<body>
<?php
    LayoutClass::return_header();
    ?>
    <div class="register">
        <form action="signup.php" method="post" class = "register__container">
            <h1>Register</h1>
            <ul>
                <li><label>User name</label>
                <input class="form_black_text" type="text" name="name" id="" placeholder="eg. user1"/></li>
                <li><label>Email</label>
                <input class="form_black_text" type="email" name="email" id="" placeholder="eg. test@test"/></li>
                <li><label>Password</label>
                <input class="form_black_text" type="password" name="password" id="" placeholder="minimum of 8 digits"/></li>
                <li><label>Repeat Password</label>
                <input class="form_black_text" type="password" name="password_rep" id="" placeholder="minimum of 8 digits"/></li>
                <li><label><a href="terms.php">I accept terms and conditions</a></label>
                <input class="form_black_text checkbox" type="checkbox" name="terms" id=""/></li>
                <li><p><?php
                    if (isset($_SESSION['signupError'])){
                        echo $_SESSION['signupError'];
                    }
                    ?>
                </p></li>
                <li><input class="form_black_text" type="submit" value="Register"/></li>
            
            </ul>
        </form>
</div>
    
    <?php
    LayoutClass::return_footer();
    ?>
</body>
</html>